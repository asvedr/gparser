package preset

import (
	gp "gitlab.com/asvedr/gparser"
	"gitlab.com/asvedr/gparser/str"
)

const digits = "0123456789"

func EOF[Ctx any]() gp.Parser[struct{}, Ctx] {
	return gp.Parser[struct{}, Ctx]{
		F: func(src []rune, _ Ctx) (*gp.Parsed[struct{}], error) {
			trimmed := str.TrimSpaceL(src)
			if len(trimmed) != 0 {
				return nil, gp.ErrExpectedEOF{Found: string(trimmed)}
			}
			return &gp.Parsed[struct{}]{}, nil
		},
	}
}

func Number[Ctx any]() gp.Parser[string, Ctx] {
	return Word[Ctx]("number", digits)
}

func Token[Ctx any](tokens ...string) gp.Parser[string, Ctx] {
	main_token := tokens[0]
	var r_tokens [][]rune
	for _, t := range tokens {
		r_tokens = append(r_tokens, []rune(t))
	}
	parser := func(src []rune, _ Ctx) (*gp.Parsed[string], error) {
		for _, r_token := range r_tokens {
			if !str.HasPrefix(src, r_token) {
				continue
			}
			rest := src[len(r_token):]
			return &gp.Parsed[string]{Value: main_token, Rest: rest}, nil
		}
		return nil, gp.ErrTokenNotFound{Token: main_token}
	}
	return gp.Parser[string, Ctx]{F: parser}
}

func Word[Ctx any](key string, alphabet string) gp.Parser[string, Ctx] {
	s_alphabet := map[rune]int{}
	for _, sym := range alphabet {
		s_alphabet[sym] = 1
	}
	parse := func(src []rune, _ Ctx) (*gp.Parsed[string], error) {
		i := 0
		var sym rune
		for ; i < len(src); i += 1 {
			sym = src[i]
			if s_alphabet[sym] != 1 {
				break
			}
		}
		if i == 0 {
			return nil, gp.ErrTokenNotFound{Token: key}
		}
		word := string(src[:i])
		rest := src[i:]
		return &gp.Parsed[string]{Value: word, Rest: rest}, nil
	}
	return gp.Parser[string, Ctx]{F: parse}
}
