package build

func Flip[A any, B any, T any](f func(A, B) T) func(B, A) T {
	return func(b B, a A) T {
		return f(a, b)
	}
}
