package join

import (
	gp "gitlab.com/asvedr/gparser"
	"gitlab.com/asvedr/gparser/str"
)

func SpacePref[T any, Ctx any](
	p gp.Parser[T, Ctx],
) gp.Parser[T, Ctx] {
	parse := func(src []rune, ctx Ctx) (*gp.Parsed[T], error) {
		src = str.TrimSpaceL(src)
		return p.F(src, ctx)
	}
	return gp.Parser[T, Ctx]{F: parse}
}

func Or[T any, Ctx any](
	parsers ...gp.Parser[T, Ctx],
) gp.Parser[T, Ctx] {
	parse := func(src []rune, ctx Ctx) (*gp.Parsed[T], error) {
		var err error
		var res *gp.Parsed[T]
		for _, p := range parsers {
			res, err = p.F(src, ctx)
			if err == nil {
				return res, nil
			}
		}
		return nil, err
	}
	return gp.Parser[T, Ctx]{F: parse}
}

func And[A any, B any, T any, Ctx any](
	parser_a gp.Parser[A, Ctx],
	parser_b gp.Parser[B, Ctx],
	combine func(A, B) T,
) gp.Parser[T, Ctx] {
	return gp.Parser[T, Ctx]{
		F: func(src []rune, ctx Ctx) (*gp.Parsed[T], error) {
			a, err := parser_a.F(src, ctx)
			if err != nil {
				return nil, err
			}
			b, err := parser_b.F(a.Rest, ctx)
			if err != nil {
				return nil, err
			}
			t := combine(a.Value, b.Value)
			return &gp.Parsed[T]{Value: t, Rest: b.Rest}, err
		},
	}
}

func And3[A any, B any, C any, T any, Ctx any](
	parser_a gp.Parser[A, Ctx],
	parser_b gp.Parser[B, Ctx],
	parser_c gp.Parser[C, Ctx],
	combine func(A, B, C) T,
) gp.Parser[T, Ctx] {
	return gp.Parser[T, Ctx]{
		F: func(src []rune, ctx Ctx) (*gp.Parsed[T], error) {
			a, err := parser_a.F(src, ctx)
			if err != nil {
				return nil, err
			}
			b, err := parser_b.F(a.Rest, ctx)
			if err != nil {
				return nil, err
			}
			c, err := parser_c.F(b.Rest, ctx)
			if err != nil {
				return nil, err
			}
			t := combine(a.Value, b.Value, c.Value)
			return &gp.Parsed[T]{Value: t, Rest: c.Rest}, err
		},
	}
}

func And4[A any, B any, C any, D any, T any, Ctx any](
	parser_a gp.Parser[A, Ctx],
	parser_b gp.Parser[B, Ctx],
	parser_c gp.Parser[C, Ctx],
	parser_d gp.Parser[D, Ctx],
	combine func(A, B, C, D) T,
) gp.Parser[T, Ctx] {
	return gp.Parser[T, Ctx]{
		F: func(src []rune, ctx Ctx) (*gp.Parsed[T], error) {
			a, err := parser_a.F(src, ctx)
			if err != nil {
				return nil, err
			}
			b, err := parser_b.F(a.Rest, ctx)
			if err != nil {
				return nil, err
			}
			c, err := parser_c.F(b.Rest, ctx)
			if err != nil {
				return nil, err
			}
			d, err := parser_d.F(c.Rest, ctx)
			if err != nil {
				return nil, err
			}
			t := combine(a.Value, b.Value, c.Value, d.Value)
			return &gp.Parsed[T]{Value: t, Rest: d.Rest}, err
		},
	}
}

func And5[A any, B any, C any, D any, E any, T any, Ctx any](
	parser_a gp.Parser[A, Ctx],
	parser_b gp.Parser[B, Ctx],
	parser_c gp.Parser[C, Ctx],
	parser_d gp.Parser[D, Ctx],
	parser_e gp.Parser[E, Ctx],
	combine func(A, B, C, D, E) T,
) gp.Parser[T, Ctx] {
	return gp.Parser[T, Ctx]{
		F: func(src []rune, ctx Ctx) (*gp.Parsed[T], error) {
			a, err := parser_a.F(src, ctx)
			if err != nil {
				return nil, err
			}
			b, err := parser_b.F(a.Rest, ctx)
			if err != nil {
				return nil, err
			}
			c, err := parser_c.F(b.Rest, ctx)
			if err != nil {
				return nil, err
			}
			d, err := parser_d.F(c.Rest, ctx)
			if err != nil {
				return nil, err
			}
			e, err := parser_e.F(d.Rest, ctx)
			if err != nil {
				return nil, err
			}
			t := combine(a.Value, b.Value, c.Value, d.Value, e.Value)
			return &gp.Parsed[T]{Value: t, Rest: e.Rest}, err
		},
	}
}

func AndOpt[A any, B any, T any, Ctx any](
	parser_a gp.Parser[A, Ctx],
	parser_b gp.Parser[B, Ctx],
	combine func(A, *B) T,
) gp.Parser[T, Ctx] {
	parse := func(src []rune, ctx Ctx) (*gp.Parsed[T], error) {
		a, err := parser_a.F(src, ctx)
		if err != nil {
			return nil, err
		}
		b, err := parser_b.F(a.Rest, ctx)
		var parsed *gp.Parsed[T]
		if err == nil {
			parsed = &gp.Parsed[T]{
				Value: combine(a.Value, &b.Value),
				Rest:  b.Rest,
			}
		} else {
			parsed = &gp.Parsed[T]{
				Value: combine(a.Value, nil),
				Rest:  a.Rest,
			}
		}
		return parsed, nil
	}
	return gp.Parser[T, Ctx]{F: parse}
}

func OptAnd[A any, B any, T any, Ctx any](
	parser_a gp.Parser[A, Ctx],
	parser_b gp.Parser[B, Ctx],
	combine func(*A, B) T,
) gp.Parser[T, Ctx] {
	parse := func(src []rune, ctx Ctx) (*gp.Parsed[T], error) {
		a, a_err := parser_a.F(src, ctx)
		if a_err == nil {
			src = a.Rest
		}
		b, err := parser_b.F(src, ctx)
		if err != nil {
			return nil, err
		}
		var res T
		if a_err == nil {
			res = combine(&a.Value, b.Value)
		} else {
			res = combine(nil, b.Value)
		}
		return &gp.Parsed[T]{Value: res, Rest: b.Rest}, nil
	}
	return gp.Parser[T, Ctx]{F: parse}
}

func AndSkip[A any, B any, Ctx any](
	parser_a gp.Parser[A, Ctx],
	parser_b gp.Parser[B, Ctx],
) gp.Parser[A, Ctx] {
	parse := func(src []rune, ctx Ctx) (*gp.Parsed[A], error) {
		a, err := parser_a.F(src, ctx)
		if err != nil {
			return nil, err
		}
		b, err := parser_b.F(a.Rest, ctx)
		if err != nil {
			return nil, err
		}
		a.Rest = b.Rest
		return a, nil
	}
	return gp.Parser[A, Ctx]{F: parse}
}

func SkipAnd[A any, B any, Ctx any](
	parser_a gp.Parser[A, Ctx],
	parser_b gp.Parser[B, Ctx],
) gp.Parser[B, Ctx] {
	parse := func(src []rune, ctx Ctx) (*gp.Parsed[B], error) {
		a, err := parser_a.F(src, ctx)
		if err != nil {
			return nil, err
		}
		return parser_b.F(a.Rest, ctx)
	}
	return gp.Parser[B, Ctx]{F: parse}
}
