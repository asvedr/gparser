package types_test

import (
	"reflect"
	"strconv"
	"testing"

	gp "gitlab.com/asvedr/gparser"
	"gitlab.com/asvedr/gparser/build"
	"gitlab.com/asvedr/gparser/join"
	"gitlab.com/asvedr/gparser/preset"
)

type Fract struct {
	Num   int
	Denom int
}

type none struct{}

func TestFract(t *testing.T) {
	p_int := gp.MapErrNoCtx(
		preset.Number[none](),
		strconv.Atoi,
	)
	parser := join.And3(
		p_int,
		preset.Token[none]("/"),
		p_int,
		func(num int, _ string, denom int) Fract {
			return Fract{Num: num, Denom: denom}
		},
	)
	fract, err := parser.Parse("1/17", none{})
	if err != nil {
		t.Fatal(err)
	}
	if !reflect.DeepEqual(*fract, Fract{Num: 1, Denom: 17}) {
		t.Fatal(fract)
	}
}

func TestOr(t *testing.T) {
	p_int := join.SpacePref(
		gp.MapErrNoCtx(
			preset.Number[none](),
			strconv.Atoi,
		),
	)
	var num_p gp.Parser[int, none] = join.SkipAnd(
		join.SpacePref(preset.Token[none]("num:")),
		p_int,
	)
	var denom_p gp.Parser[int, none] = join.SkipAnd(
		join.SpacePref(preset.Token[none]("denom:")),
		p_int,
	)
	combine := func(num int, denom int) Fract {
		return Fract{Num: num, Denom: denom}
	}
	parser := join.Or(
		join.And(num_p, denom_p, combine),
		join.And(denom_p, num_p, build.Flip(combine)),
	)

	res, err := parser.Parse("num: 2 denom: 3", none{})
	if err != nil {
		t.Fatal(err)
	}
	if !reflect.DeepEqual(*res, Fract{Num: 2, Denom: 3}) {
		t.Fatal(res)
	}

	res, err = parser.Parse("denom: 33 num: 22", none{})
	if err != nil {
		t.Fatal(err)
	}
	if !reflect.DeepEqual(*res, Fract{Num: 22, Denom: 33}) {
		t.Fatal(res)
	}
}
