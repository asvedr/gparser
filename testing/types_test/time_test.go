package types_test

import (
	"reflect"
	"strconv"
	"testing"
	"time"

	gp "gitlab.com/asvedr/gparser"
	"gitlab.com/asvedr/gparser/build"
	"gitlab.com/asvedr/gparser/join"
	"gitlab.com/asvedr/gparser/preset"
)

type t_date struct {
	yy int
	mm int
	dd int
}

type t_time struct {
	hh   int
	mm   int
	date *t_date
}

type d_m struct {
	d int
	m int
}

type ctx struct {
	now time.Time
}

func parser() gp.Parser[t_time, ctx] {
	num := gp.MapErrNoCtx(preset.Number[ctx](), strconv.Atoi)
	tm_digit_parser := join.And3[int, string, int, t_time, ctx](
		num,
		preset.Token[ctx](":"),
		num,
		func(hh int, _ string, mm int) t_time {
			return t_time{hh: hh, mm: mm}
		},
	)
	int_after_parser := join.And3(
		preset.Token[ctx]("after"),
		join.SpacePref(num),
		join.SpacePref(preset.Token[ctx]("min")),
		func(_ string, mm int, _ string) int {
			return mm
		},
	)
	tm_after_parser := gp.Map(
		int_after_parser,
		func(min int, c ctx) t_time {
			tm := c.now.Add(time.Minute * time.Duration(min))
			return t_time{
				hh: tm.Hour(),
				mm: tm.Minute(),
			}
		},
	)
	tm_parser := join.SpacePref(join.Or(tm_digit_parser, tm_after_parser))
	dot := preset.Token[ctx](".")
	dt_dd_mm_yy_parser := join.And5(
		num,
		dot,
		num,
		dot,
		num,
		func(d int, _ string, m int, _ string, y int) t_date {
			return t_date{yy: y, mm: m, dd: d}
		},
	)
	dt_dd_mm_parser_ := join.And3(
		num,
		dot,
		num,
		func(d int, _ string, m int) d_m {
			return d_m{d: d, m: m}
		},
	)
	dt_dd_mm_parser := gp.Map(dt_dd_mm_parser_, func(dm d_m, c ctx) t_date {
		return t_date{yy: c.now.Year(), mm: dm.m, dd: dm.d}
	})
	dt_shift_parser := gp.Map(
		preset.Token[ctx]("today"),
		func(_ string, c ctx) t_date {
			return t_date{
				yy: c.now.Year(),
				mm: int(c.now.Month()),
				dd: c.now.Day(),
			}
		},
	)
	dt_parser := join.SpacePref(join.Or(
		dt_dd_mm_yy_parser,
		dt_dd_mm_parser,
		dt_shift_parser,
	))
	combine := func(d t_date, t t_time) t_time {
		t.date = &d
		return t
	}
	return join.Or[t_time, ctx](
		join.And(dt_parser, tm_parser, combine),
		join.And(tm_parser, dt_parser, build.Flip(combine)),
		tm_parser,
	)
}

func TestTimeDtTm(t *testing.T) {
	now := ctx{
		now: time.Date(2020, 1, 1, 10, 2, 0, 0, time.UTC),
	}
	tm, err := parser().Parse("today 4:20", now)
	if err != nil {
		t.Fatal(err)
	}
	exp := &t_time{
		hh:   4,
		mm:   20,
		date: &t_date{yy: 2020, mm: 1, dd: 1},
	}
	if !reflect.DeepEqual(exp, tm) {
		t.Fatal(tm)
	}
}

func TestTimeTmDt(t *testing.T) {
	now := ctx{
		now: time.Date(2020, 1, 1, 10, 2, 0, 0, time.UTC),
	}
	tm, err := parser().Parse("4:15 today", now)
	if err != nil {
		t.Fatal(err)
	}
	exp := &t_time{
		hh:   4,
		mm:   15,
		date: &t_date{yy: 2020, mm: 1, dd: 1},
	}
	if !reflect.DeepEqual(exp, tm) {
		t.Fatal(tm)
	}
}

func TestTimeOnlyTm(t *testing.T) {
	now := ctx{
		now: time.Date(2020, 1, 1, 10, 2, 0, 0, time.UTC),
	}
	tm, err := parser().Parse("after 20 min", now)
	if err != nil {
		t.Fatal(err)
	}
	exp := &t_time{
		hh:   10,
		mm:   22,
		date: nil,
	}
	if !reflect.DeepEqual(exp, tm) {
		t.Fatal(tm)
	}
}
