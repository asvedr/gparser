package expr_test

import (
	"reflect"
	"strconv"
	"testing"

	gp "gitlab.com/asvedr/gparser"
	"gitlab.com/asvedr/gparser/join"
	"gitlab.com/asvedr/gparser/preset"
	"gitlab.com/asvedr/gparser/seq"
)

type none struct{}

func parser() gp.Parser[[]int, none] {
	num := gp.MapErrNoCtx(
		join.SpacePref(preset.Number[none]()),
		strconv.Atoi,
	)
	comma := join.SpacePref(preset.Token[none](","))
	num_and_comma := join.AndSkip(num, comma)
	cl := join.SpacePref(preset.Token[none]("]"))
	body_or_end := join.Or(
		gp.MapNoCtx(cl, func(_ string) []int {
			return []int{}
		}),
		join.And(
			seq.ZeroOrMore(num_and_comma),
			num,
			func(acc []int, val int) []int {
				return append(acc, val)
			},
		),
	)
	return join.SkipAnd(
		join.SpacePref(preset.Token[none]("[")),
		body_or_end,
	)
}

func TestListParser(t *testing.T) {
	p := parser()

	val, err := p.Parse("[ ]", none{})
	if err != nil {
		t.Fatal(err)
	}
	if !reflect.DeepEqual(*val, []int{}) {
		t.Fatal(val)
	}

	val, err = p.Parse("[1]", none{})
	if err != nil {
		t.Fatal(err)
	}
	if !reflect.DeepEqual(*val, []int{1}) {
		t.Fatal(val)
	}

	val, err = p.Parse("[1,2,3]", none{})
	if err != nil {
		t.Fatal(err)
	}
	if !reflect.DeepEqual(*val, []int{1, 2, 3}) {
		t.Fatal(val)
	}

	_, err = p.Parse("[1,2,]", none{})
	if err == nil {
		t.Fatal("expected err")
	}
}
