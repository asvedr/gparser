package opt_and_test

import (
	"strconv"
	"testing"

	gp "gitlab.com/asvedr/gparser"
	"gitlab.com/asvedr/gparser/join"
	"gitlab.com/asvedr/gparser/preset"
)

type none struct{}

func parser() gp.Parser[int, none] {
	var num = gp.MapErrNoCtx(
		join.SpacePref(preset.Number[none]()),
		strconv.Atoi,
	)
	neg := join.SpacePref(preset.Token[none]("-", "~"))
	pos := join.SpacePref(preset.Token[none]("+"))
	op := join.Or(neg, pos)
	combine_op_num := func(op string, num int) int {
		switch op {
		case "+":
			return num
		case "-":
			return -num
		default:
			panic("")
		}
	}
	combine_opt_op_num := func(op *string, num int) int {
		if op == nil {
			return num
		}
		switch *op {
		case "+":
			return num
		case "-":
			return -num
		default:
			panic("")
		}
	}
	return join.AndOpt(
		join.OptAnd(op, num, combine_opt_op_num),
		join.And(op, num, combine_op_num),
		func(a int, b *int) int {
			if b != nil {
				return a + *b
			}
			return a
		},
	)
}

func TestOptAnd(t *testing.T) {
	p := parser()
	val, err := p.Parse("1", none{})
	if err != nil || *val != 1 {
		t.Fatalf("%v|%v", val, err)
	}
	val, err = p.Parse("-1", none{})
	if err != nil || *val != -1 {
		t.Fatalf("%v|%v", val, err)
	}
	val, err = p.Parse("1 + 2", none{})
	if err != nil || *val != 3 {
		t.Fatalf("%v|%v", val, err)
	}
	val, err = p.Parse("-2 ~ 10", none{})
	if err != nil || *val != -12 {
		t.Fatalf("%v|%v", val, err)
	}
}
