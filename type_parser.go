package gparser

type Parsed[T any] struct {
	Value T
	Rest  []rune
}

type Parser[T any, Ctx any] struct {
	F func([]rune, Ctx) (*Parsed[T], error)
}

func F[T any, Ctx any](f func([]rune, Ctx) (*Parsed[T], error)) Parser[T, Ctx] {
	return Parser[T, Ctx]{F: f}
}

func (self Parser[T, Ctx]) Parse(src string, ctx Ctx) (*T, error) {
	runes := []rune(src)
	parsed, err := self.F(runes, ctx)
	if err != nil {
		return nil, err
	}
	return &parsed.Value, nil
}

func (self Parser[T, Ctx]) ParseAndRest(src string, ctx Ctx) (*T, string, error) {
	runes := []rune(src)
	parsed, err := self.F(runes, ctx)
	if err != nil {
		return nil, "", err
	}
	return &parsed.Value, string(parsed.Rest), nil
}

func Map[A any, B any, Ctx any](p Parser[A, Ctx], f func(A, Ctx) B) Parser[B, Ctx] {
	return Parser[B, Ctx]{
		F: func(src []rune, ctx Ctx) (*Parsed[B], error) {
			val, err := p.F(src, ctx)
			if err != nil {
				return nil, err
			}
			return &Parsed[B]{Value: f(val.Value, ctx), Rest: val.Rest}, nil
		},
	}
}

func MapNoCtx[A any, B any, Ctx any](p Parser[A, Ctx], f func(A) B) Parser[B, Ctx] {
	return Parser[B, Ctx]{
		F: func(src []rune, ctx Ctx) (*Parsed[B], error) {
			val, err := p.F(src, ctx)
			if err != nil {
				return nil, err
			}
			return &Parsed[B]{Value: f(val.Value), Rest: val.Rest}, nil
		},
	}
}

func MapErr[A any, B any, Ctx any](p Parser[A, Ctx], f func(A, Ctx) (B, error)) Parser[B, Ctx] {
	return Parser[B, Ctx]{
		F: func(src []rune, ctx Ctx) (*Parsed[B], error) {
			val, err := p.F(src, ctx)
			if err != nil {
				return nil, err
			}
			new_val, err := f(val.Value, ctx)
			if err != nil {
				return nil, err
			}
			return &Parsed[B]{Value: new_val, Rest: val.Rest}, nil
		},
	}
}

func MapErrNoCtx[A any, B any, Ctx any](p Parser[A, Ctx], f func(A) (B, error)) Parser[B, Ctx] {
	return Parser[B, Ctx]{
		F: func(src []rune, ctx Ctx) (*Parsed[B], error) {
			val, err := p.F(src, ctx)
			if err != nil {
				return nil, err
			}
			new_val, err := f(val.Value)
			if err != nil {
				return nil, err
			}
			return &Parsed[B]{Value: new_val, Rest: val.Rest}, nil
		},
	}
}
