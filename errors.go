package gparser

import "errors"

var ErrInvalidLen = errors.New("invalid len")

type ErrTokenNotFound struct{ Token string }

type ErrExpectedEOF struct{ Found string }

func (e ErrTokenNotFound) Error() string {
	return "ErrTokenNotFound(" + e.Token + ")"
}

func (e ErrExpectedEOF) Error() string {
	return "ExpectedEOFButFound(\"" + e.Found + "\")"
}
