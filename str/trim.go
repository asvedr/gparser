package str

var asciiSpace = map[rune]int{'\t': 1, '\n': 1, '\v': 1, '\f': 1, '\r': 1, ' ': 1}

func HasPrefix(src []rune, prefix []rune) bool {
	if len(src) < len(prefix) {
		return false
	}
	for i := 0; i < len(prefix); i += 1 {
		if src[i] != prefix[i] {
			return false
		}
	}
	return true
}

func TrimSpaceL(src []rune) []rune {
	for i := 0; i < len(src); i += 1 {
		if asciiSpace[src[i]] != 1 {
			return src[i:]
		}
	}
	return []rune{}
}
