package seq

import gp "gitlab.com/asvedr/gparser"

func Validate[T any, Ctx any](
	parser gp.Parser[T, Ctx],
	checker func([]T) error,
) gp.Parser[[]T, Ctx] {
	parse := func(src []rune, ctx Ctx) (*gp.Parsed[[]T], error) {
		var result []T
		for {
			val, err := parser.F(src, ctx)
			if err != nil {
				break
			}
			result = append(result, val.Value)
			src = val.Rest
		}
		err := checker(result)
		if err != nil {
			return nil, err
		}
		return &gp.Parsed[[]T]{Value: result, Rest: src}, nil
	}
	return gp.Parser[[]T, Ctx]{F: parse}
}

func ZeroOrMore[T any, Ctx any](
	parser gp.Parser[T, Ctx],
) gp.Parser[[]T, Ctx] {
	parse := func(src []rune, ctx Ctx) (*gp.Parsed[[]T], error) {
		var result []T
		for {
			val, err := parser.F(src, ctx)
			if err != nil {
				break
			}
			result = append(result, val.Value)
			src = val.Rest
		}
		return &gp.Parsed[[]T]{Value: result, Rest: src}, nil
	}
	return gp.Parser[[]T, Ctx]{F: parse}
}

func OneOrMore[T any, Ctx any](
	parser gp.Parser[T, Ctx],
) gp.Parser[[]T, Ctx] {
	return Validate(
		parser,
		func(seq []T) error {
			if len(seq) < 1 {
				return gp.ErrInvalidLen
			}
			return nil
		},
	)
}
